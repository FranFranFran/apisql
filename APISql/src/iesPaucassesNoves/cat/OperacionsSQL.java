package iesPaucassesNoves.cat;

public interface OperacionsSQL {

	public String insert();
	public String delete();
	public String update();
	
}